from django.test import TestCase, LiveServerTestCase
from django.test import TestCase, Client
from django.urls import resolve
from .views import story7, redirecting
from django.http import HttpRequest

# Create your tests here.

class UnitTestStory6(TestCase):
	def test_story7_url_is_exist(self):
		response = Client().get('/story7/')
		self.assertEqual(response.status_code, 200)
		
	def test_story7_url_is_notexist(self):
		response = Client().get('/home/')
		self.assertEqual(response.status_code, 404)

	def test_story7_using_activity_function(self):
		response = resolve('/story7/')
		self.assertEqual(response.func, story7)

	def test_story7_using_story7_template(self):
		response = Client().get('/story7/')
		self.assertTemplateUsed(response, 'index.html')

	def test_story7_page_title_is_right(self):
		request = HttpRequest()
		response = story7(request)
		html_response = response.content.decode('utf8')
		self.assertIn('<title>Manuel Yoseph Ray</title>', html_response)


	def test_story7_redirecting(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 302)