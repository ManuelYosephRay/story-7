from django.contrib import admin
from django.urls import path
from . import views

urlpatterns = [
    path('story7/', views.story7, name='story7'),
    path('', views.redirecting, name='redirecting'),
]
