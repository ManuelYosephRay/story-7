from django.shortcuts import render, redirect

def story7(request):
    return render(request, 'index.html')

def redirecting(request):
	return redirect('/story7/')
